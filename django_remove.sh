#!/usr/bin/env bash

# based on:
# http://michal.karzynski.pl/blog/2013/06/09/django-nginx-gunicorn-virtualenv-supervisor/

_APPNAME_='testing'

## Nginx
sudo rm /etc/nginx/sites-enabled/$_APPNAME_
sudo service nginx restart 
sudo rm /etc/nginx/sites-available/$_APPNAME_

## Supervisor
sudo supervisorctl stop $_APPNAME_
sudo rm /etc/supervisor/conf.d/$_APPNAME_.conf

## del folder
sudo rm -r "/webapps/${_APPNAME_}_django"
sudo userdel $_APPNAME_