#!/usr/bin/env bash

# based on:
# http://michal.karzynski.pl/blog/2013/06/09/django-nginx-gunicorn-virtualenv-supervisor/

APPNAME='testing'
SERVERNAME='rodrigo.tk'

# CONSTS
_HOME_="/webapps/${APPNAME}_django/"
_PYTHON_="/webapps/${APPNAME}_django/bin/python"
_PIP_="/webapps/${APPNAME}_django/bin/pip"

## Application user

if id -g 'webapps' >/dev/null 2>&1; then
    echo "user webapps exists"
else
    echo "creating group webapps"
    sudo groupadd --system webapps
fi

if id -u $APPNAME >/dev/null 2>&1; then
    echo "user $APPNAME exists"
else
    echo "creating user: "$APPNAME
    sudo useradd --system --gid webapps --shell /bin/bash --home $_HOME_ $APPNAME
fi

## Install virtualenv and create an environment for you app
sudo apt-get install --yes python-setuptools
sudo easy_install virtualenv

## Create and activate an environment for your application
sudo mkdir -p $_HOME_
sudo chown $APPNAME:webapps $_HOME_
sudo chmod g+s $_HOME_
sudo chmod g+w $_HOME_

sudo su $APPNAME -c "cp /etc/skel/.* ${_HOME_}"
sudo su $APPNAME -c "echo 'chmod -R g+w ~/${APPNAME}' >> ~/.bashrc"

sudo su $APPNAME -c "virtualenv ${_HOME_}"
sudo su $APPNAME -c "${_HOME_}/bin/pip install django"
sudo su $APPNAME -c "mkdir -p ${_HOME_}/${APPNAME}"
sudo su $APPNAME -c "${_HOME_}/bin/django-admin startproject ${APPNAME} ${_HOME_}/${APPNAME}"

## Gunicorn
sudo su $APPNAME -c "${_PIP_} install gunicorn"
sudo su $APPNAME -c "cp gunicorn_start ${_HOME_}/bin"
sudo su $APPNAME -c "sed -i.bak s/APPNAME/${APPNAME}/g ${_HOME_}/bin/gunicorn_start"
sudo su $APPNAME -c "chmod u+x ${_HOME_}bin/gunicorn_start"

sudo apt-get install --yes python-dev
sudo su $APPNAME -c "${_PIP_} install setproctitle"

## Supervisor
sudo apt-get install --yes supervisor
sudo cp APPNAME.supervisorconf /etc/supervisor/conf.d/$APPNAME.conf
sudo sed -i.bak s/APPNAME/${APPNAME}/g /etc/supervisor/conf.d/$APPNAME.conf

sudo su $APPNAME -c "mkdir -p ${_HOME_}/logs/"
sudo su $APPNAME -c "touch ${_HOME_}/logs/gunicorn_supervisor.log"

sudo supervisorctl reread
sudo supervisorctl update

## Nginx
sudo apt-get install --yes nginx
sudo service nginx start
sudo cp APPNAME.nginxconf /etc/nginx/sites-available/$APPNAME
sudo sed -i.bak s/APPNAME/${APPNAME}/g /etc/nginx/sites-available/$APPNAME
sudo sed -i.bak s/SERVERNAME/${SERVERNAME}/g /etc/nginx/sites-available/$APPNAME
sudo ln -s /etc/nginx/sites-available/$APPNAME /etc/nginx/sites-enabled/$APPNAME
sudo service nginx restart

## Final Dirs
sudo su $APPNAME -c "mkdir -p ${_HOME_}/media"
sudo su $APPNAME -c "mkdir -p ${_HOME_}/static"

sudo su $APPNAME -c "mkdir -p ${_HOME_}/${APPNAME}/apps"
sudo su $APPNAME -c "touch    ${_HOME_}/${APPNAME}/apps/__init__.py"
sudo su $APPNAME -c "mkdir -p ${_HOME_}/${APPNAME}/bin"
sudo su $APPNAME -c "mkdir -p ${_HOME_}/${APPNAME}/static"
sudo su $APPNAME -c "mkdir -p ${_HOME_}/${APPNAME}/templates"

## local_settings.py
sudo su $APPNAME -c "cp local_settings.py ${_HOME_}/${APPNAME}/${APPNAME}"
sudo su $APPNAME -c "sed -i.bak s/APPNAME/${APPNAME}/g ${_HOME_}/${APPNAME}/${APPNAME}/local_settings.py"
sudo su $APPNAME -c "${_PIP_} install django-extensions django-registration"

sudo su $APPNAME -c "echo 'try:'                               >> ${_HOME_}/${APPNAME}/${APPNAME}/settings.py"
sudo su $APPNAME -c "echo '    from local_settings import *'   >> ${_HOME_}/${APPNAME}/${APPNAME}/settings.py"
sudo su $APPNAME -c "echo 'except ImportError, e:'             >> ${_HOME_}/${APPNAME}/${APPNAME}/settings.py"
sudo su $APPNAME -c "echo '    pass'                           >> ${_HOME_}/${APPNAME}/${APPNAME}/settings.py"

sudo su $APPNAME -c "${_PYTHON_} ${_HOME_}/${APPNAME}/manage.py syncdb"
sudo su $APPNAME -c "${_PYTHON_} ${_HOME_}/${APPNAME}/manage.py collectstatic"

# # PostgreSQL
# sudo apt-get install --yes postgresql postgresql-contrib
# # dependencias psycopg2
# sudo apt-get install --yes libpq-dev python-dev
# # virtualenv, supervisor y nginx
# sudo apt-get install --yes python-virtualenv, supervisor, nginx

# echo ""
# echo "creando usuario sidmil en postgres"
# echo "# -----------------------------"
# echo "# ingresar password            "
# echo "#   micomo.django.2013         "
# echo "#                              "
# echo "# todas las opciones n         "
# echo "# -----------------------------"
# echo ""
# sudo su postgres -c "createuser -P micomo_django"
# sudo su postgres -c "createdb --owner micomo_django db_micomo_django"

