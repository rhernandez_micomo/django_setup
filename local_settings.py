import os
from django.conf import settings

# setting variables
BASE_DIR = settings.BASE_DIR
ROOT_DIR = os.path.normpath(os.path.join(BASE_DIR,'..'))
INSTALLED_APPS = settings.INSTALLED_APPS

# mis aplicaciones
# INSTALLED_APPS += (
#     'apps.sidmil',
# )

# database
# DATABASES = {
#     'default': {
#         'ENGINE':'django.db.backends.postgresql_psycopg2',
#         'NAME': 'db_APPNAME_django',
#         'USER': 'APPNAME_django',
#         'PASSWORD': 'PSQL_STRONG_PASSWORD',
#         'HOST': 'localhost',
#         'PORT': '',
#     }
# }

# static, media and templates
STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(ROOT_DIR,'static')
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(ROOT_DIR,'media')

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, 'templates'),
)

# you can set other configs
